Heterogeneous Computing - Thematic CERN School of Computing
============

Welcome to the Heterogeneous Computing lab session of the thematic CERN School of Computing.

Open the jupyter notebook containing the exercises `exercises.ipynb` by clicking on it and follow the instructions there.

Below are the links to the files that require editing during the exercise. Click on them to open them in a separate browser tab.

Exercises:

* [device\_properties/device\_properties.cu](device_properties/device_properties.cu)

* [hello\_world/hello\_world.cu](hello_world/hello_world.cu)

* [vector\_addition/vector\_addition.cu](vector_addition/vector_addition.cu)

* [matrix\_multiplication/matrix\_multiply.cu](matrix_multiplication/matrix_multiply.cu)

* [matrix\_multiplication/matrix\_multiply\_threads.cu](matrix_multiplication/matrix_multiply_threads.cu)

* [matrix\_multiplication/matrix\_multiply\_grid.cu](matrix_multiplication/matrix_multiply_grid.cu)

* [matrix\_multiplication/matrix\_multiply\_shared.cu](matrix_multiplication/matrix_multiply_shared.cu)

Solutions:

* [hello\_world/hello\_world\_solution.cu](hello_world/hello_world_solution.cu)

* [vector\_addition/vector\_addition\_solution.cu](vector_addition/vector_addition_solution.cu)

* [matrix\_multiplication/matrix\_multiply\_threads\_solution.cu](matrix_multiplication/matrix_multiply_threads_solution.cu)

* [matrix\_multiplication/matrix\_multiply\_grid\_solution.cu](matrix_multiplication/matrix_multiply_grid_solution.cu)

* [matrix\_multiplication/matrix\_multiply\_shared\_solution.cu](matrix_multiplication/matrix_multiply_shared_solution.cu)

Enjoy!
